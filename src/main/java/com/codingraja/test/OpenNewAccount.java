package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Account;
import com.codingraja.domain.CurrentAccount;
import com.codingraja.domain.LoanAccount;
import com.codingraja.domain.SavingAccount;

public class OpenNewAccount {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		//Create Accounts
		Account account = new Account(10000.0, 1001L);
		SavingAccount savingAccount = new SavingAccount(20000.0, 1002L, 4.0);
		CurrentAccount currentAccount = new CurrentAccount(30000.0, 1003L, 10000.0, 100L);
		LoanAccount loanAccount = new LoanAccount(40000.0, 1004L, 12.5, 5500.0, 35000.0);
		
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		long accNo = (Long)session.save(account);
		long savingAccountNo = (Long)session.save(savingAccount);
		long currentAccountNo = (Long)session.save(currentAccount);
		long loanAccountNo = (Long)session.save(loanAccount);
		transaction.commit();
		session.close();
		
		System.out.println("Account No: "+accNo);
		System.out.println("Saving Account No: "+savingAccountNo);
		System.out.println("Current Account No: "+currentAccountNo);
		System.out.println("Loan Account No: "+loanAccountNo);
	}

}
